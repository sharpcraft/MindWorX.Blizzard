﻿using System;

namespace MindWorX.Blizzard
{
    [Flags]
    public enum TerrainMapEdgeFlags : byte
    {
        None = 0,

        Boundary = 1 << 0,
        Flag1 = 1 << 1,

        All = Boundary | Flag1
    }
}
