﻿using System;
using System.Drawing;
using System.IO;
using System.Text;

namespace MindWorX.Blizzard
{
    public class PathingMap
    {
        private static readonly uint MagicNumber = BitConverter.ToUInt32(Encoding.ASCII.GetBytes("MP3W"), 0);

        private static PathingMap LoadVersion0(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                var width = reader.ReadInt32();
                var height = reader.ReadInt32();

                var pathMap = new PathingMap(width, height);
                for (var y = height - 1; y >= 0; y--)
                    for (var x = 0; x < width; x++)
                        pathMap[x, y] = (Pathing)reader.ReadByte();
                return pathMap;
            }
        }

        public static PathingMap Load(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                if (reader.ReadUInt32() != MagicNumber)
                    throw new InvalidOperationException("Invalid magic number.");

                switch (reader.ReadUInt32())
                {
                    case 0:
                        return LoadVersion0(stream);
                    default:
                        throw new InvalidOperationException("Invalid version number. Only version 0 is supported.");
                }
            }
        }

        public static PathingMap Load(string fileName)
        {
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                return Load(stream);
        }

        private readonly Pathing[,] pathing;

        public PathingMap(Pathing[,] pathing)
        {
            this.pathing = pathing;
        }

        public PathingMap(int width, int height)
            : this(new Pathing[width, height])
        { }

        public int Width => this.pathing.GetLength(0);

        public int Height => this.pathing.GetLength(1);

        public Pathing this[int x, int y]
        {
            get => this.pathing[x, y];
            set => this.pathing[x, y] = value;
        }

        public void Save(Stream stream)
        {
            using (var writer = new BinaryWriter(stream, Encoding.ASCII, true))
            {
                writer.Write(MagicNumber);
                writer.Write(0); // version
                writer.Write(this.Width);
                writer.Write(this.Height);
                for (var y = this.Height - 1; y >= 0; y--)
                    for (var x = 0; x < this.Width; x++)
                        writer.Write((byte)this[x, y]);
            }
        }

        public void Save(string fileName)
        {
            using (var stream = File.Open(fileName, FileMode.Create, FileAccess.Write, FileShare.Write))
                this.Save(stream);
        }

        public Image ToPathingImage()
        {
            var image = new Bitmap(this.Width, this.Height);

            for (var y = 0; y < this.Height; y++)
            {
                for (var x = 0; x < this.Width; x++)
                {
                    var r = this[x, y].HasFlagFast(Pathing.BlockWalking) ? (byte)255 : (byte)0;
                    var g = this[x, y].HasFlagFast(Pathing.BlockFlying) ? (byte)255 : (byte)0;
                    var b = this[x, y].HasFlagFast(Pathing.BlockBuilding) ? (byte)255 : (byte)0;
                    image.SetPixel(x, y, Color.FromArgb(255, r, g, b));
                }
            }

            return image;
        }
    }
}
