﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MindWorX.Blizzard
{
    public class Terrain
    {
        public static Terrain Load(string path)
        {
            using (var stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                var terrain = new Terrain();

                var magicNumber = Encoding.ASCII.GetString(reader.ReadBytes(4));
                if (magicNumber != "W3E!")
                    throw new FileLoadException("Invalid magic number in header.");
                terrain.Version = reader.ReadInt32();
                if (terrain.Version != 11)
                    throw new FileLoadException("Only supports loading version 11.");
                terrain.Tileset = (Tileset)reader.ReadByte();
                terrain.IsUsingCustomTileset = reader.ReadInt32() == 1;
                var numberOfTilesets = reader.ReadInt32();
                for (var i = 0; i < numberOfTilesets; i++)
                    terrain.Tilesets.Add(new ObjectIdB(reader.ReadInt32()));
                var numberOfCliffs = reader.ReadInt32();
                for (var i = 0; i < numberOfCliffs; i++)
                    terrain.Cliffs.Add(new ObjectIdB(reader.ReadInt32()));
                var width = reader.ReadInt32();
                var height = reader.ReadInt32();
                terrain.CenterX = reader.ReadSingle();
                terrain.CenterY = reader.ReadSingle();
                terrain.TerrainVertices = new TerrainVertex[width, height];
                for (var y = height - 1; y >= 0; y--)
                {
                    for (var x = 0; x < width; x++)
                    {
                        terrain.TerrainVertices[x, y] = TerrainVertex.FromBytes(reader.ReadBytes(7));
                    }
                }

                return terrain;
            }
        }

        public int Version { get; set; }

        public Tileset Tileset { get; set; }

        public bool IsUsingCustomTileset { get; set; }

        public List<ObjectIdB> Tilesets { get; } = new List<ObjectIdB>();

        public List<ObjectIdB> Cliffs { get; } = new List<ObjectIdB>();

        public TerrainVertex[,] TerrainVertices { get; set; }

        public int Width => this.TerrainVertices.GetLength(0);

        public int Height => this.TerrainVertices.GetLength(1);

        public float CenterX { get; set; }

        public float CenterY { get; set; }

        public void Save(string path)
        {
            using (var stream = File.Open(path, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var writer = new BinaryWriter(stream, Encoding.ASCII, true))
            {
                writer.Write(Encoding.ASCII.GetBytes("W3E!"));
                writer.Write(this.Version);
                writer.Write((byte)this.Tileset);
                writer.Write(this.IsUsingCustomTileset ? 1 : 0);
                writer.Write(this.Tilesets.Count);
                foreach (var objectId in this.Tilesets)
                    writer.Write((int)objectId);
                writer.Write(this.Cliffs.Count);
                foreach (var objectId in this.Cliffs)
                    writer.Write((int)objectId);
                writer.Write(this.Width);
                writer.Write(this.Height);
                writer.Write(this.CenterX);
                writer.Write(this.CenterY);
                for (var y = this.Height - 1; y >= 0; y--)
                {
                    for (var x = 0; x < this.Height; x++)
                    {
                        writer.Write(this.TerrainVertices[x, y].ToBytes());
                    }
                }
            }
        }
    }
}
