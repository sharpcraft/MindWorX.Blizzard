﻿using System;

namespace MindWorX.Blizzard
{
    [Flags]
    public enum Pathing : byte
    {
        Unknown1 = 1 << 0,
        BlockWalking = 1 << 1,
        BlockFlying = 1 << 2,
        BlockBuilding = 1 << 3,
        Unknown2 = 1 << 4,
        Blighted = 1 << 5,
        NoWater = 1 << 6,
        Unknown3 = 1 << 7,
    }

    public static class PathingExtensions
    {
        public static bool HasFlagFast(this Pathing value, Pathing flag)
        {
            return (value & flag) != 0;
        }
    }
}
