﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Blizzard.Storm
{
    public unsafe class SStr
    {
        public const string LIBRARY_NAME = "Storm.dll";

        public static IntPtr Handle
        {
            get
            {
                var handle = Kernel32.GetModuleHandle(LIBRARY_NAME);
                return handle != IntPtr.Zero ? handle : Kernel32.LoadLibrary(LIBRARY_NAME);
            }
        }

        [DllImport(LIBRARY_NAME, EntryPoint = "#590")]
        public static extern int HashHT([In] char* input);

        [DllImport(LIBRARY_NAME, EntryPoint = "#590")]
        public static extern int HashHT([In] string input);


        private SStr() { }
    }
}
