﻿using MindWorX.Unmanaged.Windows;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace MindWorX.Blizzard.Storm
{
    public unsafe struct SFile
    {
        public const string LIBRARY_NAME = "Storm.dll";

        public static IntPtr Handle
        {
            get
            {
                var handle = Kernel32.GetModuleHandle(LIBRARY_NAME);
                return handle != IntPtr.Zero ? handle : Kernel32.LoadLibrary(LIBRARY_NAME);
            }
        }

        [DllImport(LIBRARY_NAME, EntryPoint = "#252")]
        public static extern bool CloseArchive(IntPtr archive);

        [DllImport(LIBRARY_NAME, EntryPoint = "#253")]
        public static extern bool CloseFile(IntPtr file);

        [DllImport(LIBRARY_NAME, EntryPoint = "#265")]
        public static extern int GetFileSize(IntPtr file, out int fileSizeHigh);

        [DllImport(LIBRARY_NAME, EntryPoint = "#266")]
        public static extern int OpenArchive(string filename, int priority, int flags, out IntPtr archive);

        [DllImport(LIBRARY_NAME, EntryPoint = "#268")]
        public static extern bool OpenFileEx(IntPtr archive, string filename, uint mode, out IntPtr file);

        [DllImport(LIBRARY_NAME, EntryPoint = "#269")]
        public static extern int ReadFile(IntPtr file, IntPtr buffer, int numberOfBytesToRead, out int numberOfBytesRead, int distanceToMoveHigh = 0);

        [DllImport(LIBRARY_NAME, EntryPoint = "#269")]
        public static extern int SetFilePointer(IntPtr file, int filePosition, ref int filePositionHigh, uint moveMethod);

        public static int GetFileSize(IntPtr file)
        {
            int high;
            return GetFileSize(file, out high);
        }

        public static long GetFileSizeLong(IntPtr file)
        {
            int high;
            var low = GetFileSize(file, out high);
            return unchecked(((long)high << 32 | (uint)low));
        }

        public static Archive OpenArchive(string filename, int priority = 0, int flags = 0)
        {
            IntPtr archive;
            OpenArchive(filename, priority, flags, out archive);
            return new Archive(archive);
        }

        public static ReadOnlySFileStream Open(string filename) => new ReadOnlySFileStream(OpenFileEx(filename));

        public static ReadOnlySFileStream Open(string filename, uint mode) => new ReadOnlySFileStream(OpenFileEx(filename, mode));

        public static IntPtr OpenFileEx(string filename) => OpenFileEx(IntPtr.Zero, filename, 0);

        public static IntPtr OpenFileEx(string filename, uint mode) => OpenFileEx(IntPtr.Zero, filename, mode);

        public static IntPtr OpenFileEx(IntPtr archive, string filename, uint mode)
        {
            IntPtr result;
            return OpenFileEx(archive, filename, mode, out result) ? result : IntPtr.Zero;
        }

        public static int ReadFile(IntPtr file, byte[] buffer, int numberOfBytesToRead, out int numberOfBytesRead, int distanceToMoveHigh = 0)
        {
            fixed (byte* pBuffer = buffer)
                return ReadFile(file, new IntPtr(pBuffer), numberOfBytesToRead, out numberOfBytesRead);
        }

        public static byte[] ReadFileToEnd(IntPtr file)
        {
            int read;
            int size = GetFileSize(file);
            var buffer = Marshal.AllocHGlobal(size);
            ReadFile(file, buffer, size, out read);
            var output = new byte[size];
            Marshal.Copy(buffer, output, 0, size);
            Marshal.FreeHGlobal(buffer);
            return output;
        }

        public static int SetFilePointer(IntPtr file, int filePosition, ref int filePositionHigh, SeekOrigin moveMethod)
        {
            return SetFilePointer(file, filePosition, ref filePositionHigh, (uint)moveMethod);
        }

        public static int SetFilePointer(IntPtr file, int filePosition, uint moveMethod)
        {
            var filePositionHigh = 0;
            return SetFilePointer(file, filePosition, ref filePositionHigh, moveMethod);
        }

        public static int SetFilePointer(IntPtr file, int filePosition, SeekOrigin moveMethod)
        {
            return SetFilePointer(file, filePosition, (uint)moveMethod);
        }

        public static long SetFilePointerLong(IntPtr file, long filePosition, uint moveMethod)
        {
            var high = (int)(filePosition >> 32);
            var low = (int)(filePosition & uint.MaxValue);

            low = SetFilePointer(file, low, ref high, moveMethod);

            return unchecked(((long)high << 32 | (uint)low));
        }

        public static long SetFilePointerLong(IntPtr file, long filePosition, SeekOrigin moveMethod)
        {
            return SetFilePointerLong(file, filePosition, (uint)moveMethod);
        }
    }
}
