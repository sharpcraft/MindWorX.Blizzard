﻿namespace MindWorX.Blizzard
{
    public enum Tileset : byte
    {
        Ashenvale = (byte)'A',
        Barrens = (byte)'B',
        LordaeronFall = (byte)'F',
        LordaeronSummer = (byte)'L',
        Northrend = (byte)'N',
        Village = (byte)'V',
        LordaeronWinter = (byte)'W',
        CityDalaran = (byte)'X',
        CityLordaeron = (byte)'Y'
    }
}
