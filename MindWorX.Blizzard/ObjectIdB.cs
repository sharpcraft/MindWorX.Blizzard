﻿using System;
using System.Text;

namespace MindWorX.Blizzard
{
    /// <summary>
    /// A big-endian encoded ObjectId.
    /// </summary>
    [Serializable]
    public struct ObjectIdB
    {
        public static ObjectIdB Parse(string id)
        {
            if (id.Length != 4)
                throw new FormatException("Invalid length. Must be 4 characters long.");
            try
            {
                var bytes = Encoding.ASCII.GetBytes(id);
                return new ObjectIdB(bytes[0] | (bytes[1] << 8) | (bytes[2] << 16) | (bytes[3] << 24));
            }
            catch (Exception e)
            {
                throw new FormatException("Parsing failed. See inner exception for details.", e);
            }
        }

        public static bool TryParse(string id, out ObjectIdB objectId)
        {
            objectId = default(ObjectIdB);
            if (id.Length != 4)
                return false;
            try
            {
                var bytes = Encoding.ASCII.GetBytes(id);
                objectId = new ObjectIdB(bytes[0] | (bytes[1] << 8) | (bytes[2] << 16) | (bytes[3] << 24));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private readonly int value;

        public ObjectIdB(int id)
        {
            this.value = id;
        }

        public ObjectIdB(string id)
        {
            if (id.Length != 4)
                throw new ArgumentOutOfRangeException(nameof(id), "Invalid id. Must be 4 characters long");

            this.value = (id[0]) | (id[1] << 8) | (id[2] << 16) | (id[3] << 24);
        }

        // Explicit conversion from ObjectIdB to Int32
        public static explicit operator int(ObjectIdB from)
        {
            return from.value;
        }

        // Explicit conversion from Int32 to ObjectIdB
        public static explicit operator ObjectIdB(int from)
        {
            return new ObjectIdB(from);
        }

        // Explicit conversion from ObjectIdL to ObjectIdB
        public static explicit operator ObjectIdB(ObjectIdL from)
        {
            var bytes = BitConverter.GetBytes((int)from);
            Array.Reverse(bytes);
            return new ObjectIdB(BitConverter.ToInt32(bytes, 0));
        }

        public override string ToString()
        {
            var bytes = BitConverter.GetBytes(this.value);

            return Encoding.ASCII.GetString(bytes);
        }
    }
}
