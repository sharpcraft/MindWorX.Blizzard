﻿using System;

namespace MindWorX.Blizzard
{
    [Flags]
    public enum TerrainFlags : byte
    {
        None = 0,

        Ramp = 1 << 0,
        Blight = 1 << 1,
        Water = 1 << 2,
        Boundary = 1 << 3,

        All = Ramp | Blight | Water | Boundary
    }
}
