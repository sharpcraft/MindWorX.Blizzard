﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;

namespace MindWorX.Blizzard
{
    public struct TerrainVertex
    {
        private ushort waterLevel;

        private TerrainMapEdgeFlags mapEdge;

        private byte groundIndex;

        private TerrainFlags flags;

        private byte cliffIndex;

        private byte layerHeight;

        public static TerrainVertex FromBytes(byte[] bytes)
        {
            if (bytes.Length != 7)
                throw new ArgumentOutOfRangeException($"{nameof(bytes)}.{nameof(bytes.Length)}", "Must be exactly 7 bytes in length.");

            var vertex = new TerrainVertex();

            vertex.TerrainHeight = BitConverter.ToInt16(bytes, 0); // 16-bit
            var u16 = BitConverter.ToUInt16(bytes, 2);
            vertex.waterLevel = (ushort)(u16 & 0x3FFF); // 14-bit
            vertex.mapEdge = (TerrainMapEdgeFlags)((u16 & 0xC000) >> 14); // 2-bit
            vertex.groundIndex = (byte)(bytes[4] & 0x0F); // 4-bit
            vertex.flags = (TerrainFlags)(byte)((bytes[4] & 0xF0) >> 4); // 4-bit
            vertex.Variation = bytes[5]; // 8-bit
            vertex.cliffIndex = (byte)(bytes[6] & 0x0F); // 4-bit
            vertex.layerHeight = (byte)((bytes[6] & 0xF0) >> 4); // 4-bit

            return vertex;
        }

        public short TerrainHeight { get; set; }

        public ushort WaterLevel
        {
            get { return this.waterLevel; }
            set
            {
                if (value > 0x3FFF)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 16383.");
                this.waterLevel = value;
            }
        }

        public TerrainMapEdgeFlags MapEdge
        {
            get { return this.mapEdge; }
            set
            {
                var valueByte = (byte)value;
                if (valueByte > 0x0003)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 3.");
                this.mapEdge = value;
            }
        }

        public byte GroundIndex
        {
            get { return this.groundIndex; }
            set
            {
                if (value > 0x000F)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 15.");
                this.groundIndex = value;
            }
        }

        public TerrainFlags Flags
        {
            get { return this.flags; }
            set
            {
                var valueByte = (byte)value;
                if (valueByte > 0x000F)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 15.");
                this.flags = value;
            }
        }

        public byte Variation { get; set; }

        public byte CliffIndex
        {
            get { return this.cliffIndex; }
            set
            {
                if (value > 0x000F)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 15.");
                this.cliffIndex = value;
            }
        }

        public byte LayerHeight
        {
            get { return this.layerHeight; }
            set
            {
                if (value > 0x000F)
                    throw new ArgumentOutOfRangeException(nameof(value), "Must must be less than or equal to 15.");
                this.layerHeight = value;
            }
        }

        public byte[] ToBytes()
        {
            var bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes(this.TerrainHeight));
            bytes.AddRange(BitConverter.GetBytes((ushort)(this.waterLevel | ((ushort)this.mapEdge << 14))));
            bytes.Add((byte)(this.groundIndex | ((ushort)this.flags << 4)));
            bytes.Add(this.Variation);
            bytes.Add((byte)(this.cliffIndex | (this.layerHeight << 4)));
            return bytes.ToArray();
        }
    }
}
