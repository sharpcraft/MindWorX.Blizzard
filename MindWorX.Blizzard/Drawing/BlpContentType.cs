﻿namespace MindWorX.Blizzard.Drawing
{
    public enum BlpContentType : uint
    {
        Jpeg = 0,
        Direct = 1
    }
}
