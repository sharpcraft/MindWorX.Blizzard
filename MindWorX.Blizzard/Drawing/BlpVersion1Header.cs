﻿using System;
using System.IO;
using System.Text;

namespace MindWorX.Blizzard.Drawing
{
    public struct BlpVersion1Header
    {
        public static BlpVersion1Header Load(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                return new BlpVersion1Header((BlpContentType)reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadUInt32(), reader.ReadUInt32());
            }
        }

        public BlpVersion1Header(BlpContentType content, uint alphaBits, int width, int height, uint extra, uint hasMipmaps)
        {
            this.Content = content;
            this.AlphaBits = alphaBits;
            this.Width = width;
            this.Height = height;
            this.Extra = extra;
            this.HasMipmaps = hasMipmaps;
        }

        public uint Magic => BitConverter.ToUInt32(Encoding.ASCII.GetBytes("BLP1"), 0);

        public BlpContentType Content { get; }

        public uint AlphaBits { get; }

        public int Width { get; }

        public int Height { get; }

        public uint Extra { get; }

        public uint HasMipmaps { get; }
    }
}
