﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using BitMiracle.LibJpeg;

namespace MindWorX.Blizzard.Drawing
{
    public class BlpImage : IReadOnlyList<Image>
    {
        private static readonly uint MagicNumberVersion0 = BitConverter.ToUInt32(Encoding.ASCII.GetBytes("BLP0"), 0);

        private static readonly uint MagicNumberVersion1 = BitConverter.ToUInt32(Encoding.ASCII.GetBytes("BLP1"), 0);

        private static readonly uint MagicNumberVersion2 = BitConverter.ToUInt32(Encoding.ASCII.GetBytes("BLP2"), 0);

        private static BlpImage LoadVersion0(Stream stream) => throw new NotImplementedException("Version 0 not implemented.");

        private static BlpImage LoadVersion1(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                // Header
                var header = BlpVersion1Header.Load(stream);

                // MipmapLocator
                var mmOffsets = new uint[16];
                for (var i = 0; i < mmOffsets.Length; i++)
                    mmOffsets[i] = reader.ReadUInt32();
                var mmSizes = new uint[16];
                for (var i = 0; i < mmSizes.Length; i++)
                    mmSizes[i] = reader.ReadUInt32();

                switch (header.Content)
                {
                    case BlpContentType.Jpeg:
                        var jpegHeaderSize = reader.ReadUInt32();

                        var offset = mmOffsets[0];
                        var size = mmSizes[0];

                        var jpegBuffer = new List<byte>();
                        jpegBuffer.AddRange(reader.ReadBytes((int)jpegHeaderSize));
                        stream.Seek(offset, SeekOrigin.Begin);
                        jpegBuffer.AddRange(reader.ReadBytes((int)size));
                        using (var jpeg = new JpegImage(new MemoryStream(jpegBuffer.ToArray())))
                        {
                            var image = new Bitmap(jpeg.Width, jpeg.Height);
                            for (var y = 0; y < image.Height; y++)
                            {
                                var row = jpeg.GetRow(y);
                                for (var x = 0; x < image.Width; x++)
                                {
                                    image.SetPixel(x, y, Color.FromArgb(row[x][3], row[x][2], row[x][1], row[x][0]));
                                }
                            }
                            return new BlpImage(image.Width, image.Height, image);
                        }
                    case BlpContentType.Direct:
                        throw new NotImplementedException("Direct content not implemented.");
                }
            }

            throw new NotImplementedException();
        }

        private static BlpImage LoadVersion2(Stream stream) => throw new NotImplementedException("Version 2 not implemented.");

        public static BlpImage Load(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                var magic = reader.ReadUInt32();

                if (magic == MagicNumberVersion0)
                    return LoadVersion0(stream);

                if (magic == MagicNumberVersion1)
                    return LoadVersion1(stream);

                if (magic == MagicNumberVersion2)
                    return LoadVersion2(stream);

                throw new InvalidOperationException($"Unexpected magic number: '{magic}'");
            }
        }

        public static BlpImage Load(string fileName)
        {
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                return Load(stream);
        }


        private readonly Image[] images;

        public BlpImage(int width, int height, Image image)
                : this(width, height, new[] { image })
        { }

        public BlpImage(int width, int height, Image[] images)
        {
            if (images.Length != 1 && images.Length != 16)
                throw new ArgumentException("Must be exactly 1 image or 16 images in the array.", nameof(images));
            this.images = images;
            this.Width = width;
            this.Height = height;
        }

        public int Width { get; }

        public int Height { get; }

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator() => this.images.GetEnumerator();

        #endregion

        #region IEnumerable<Image>

        public IEnumerator<Image> GetEnumerator() => ((IEnumerable<Image>)this.images).GetEnumerator();

        #endregion

        #region IReadOnlyCollection<Image>

        public int Count => this.images.Length;

        #endregion

        #region IReadOnlyList<Image>

        public Image this[int index] => this.images[index];

        #endregion
    }
}
